const limitFunctionCallCount = require('../project/limitFunctionCallCount.cjs');

// Test limiting function calls
const limitedFunction = limitFunctionCallCount((a, b) => a + b, 2);
console.log(limitedFunction(1, 2)); // Output: 3
console.log(limitedFunction(2, 3)); // Output: 5
console.log(limitedFunction(3, 4)); // Output: null