const cacheFunction = require('../project/cacheFunction.cjs');

// Test caching function calls
const cachedFunction = cacheFunction((a, b) => a * b);
console.log(cachedFunction(2, 3)); // Output: 6 (invokes cb)
console.log(cachedFunction(2, 3)); // Output: 6 (returns cached result)
console.log(cachedFunction(4, 5)); // Output: 20 (invokes cb)
console.log(cachedFunction(4, 5)); // Output: 20 (returns cached result)