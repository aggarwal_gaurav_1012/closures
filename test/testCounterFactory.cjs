const counterFactory = require('../project/counterFactory.cjs');

// Test increment and decrement methods
const counter = counterFactory();
console.log(counter.increment()); // Output: 1
console.log(counter.increment()); // Output: 2
console.log(counter.decrement()); // Output: 1
console.log(counter.decrement()); // Output: 0