function cacheFunction(cb) {
    const cache = {};

    return function(...args) {
        const key = JSON.stringify(args);
        if (cache[key] === undefined) {
            cache[key] = cb(...args);
        }
        return cache[key];
    };
}

module.exports = cacheFunction;